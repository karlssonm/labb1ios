//
//  AppDelegate.h
//  AppenOmMigSjälv
//
//  Created by Mattias k on 2015-01-25.
//  Copyright (c) 2015 Mattias k. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

